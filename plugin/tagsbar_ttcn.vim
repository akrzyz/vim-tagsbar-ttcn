"tagsbar for ttcn
let g:tagbar_type_ttcn = {
    \ 'ctagstype' : 'TTCN',
    \ 'kinds' : [
        \ 'M:module',
        \ 'C:testcase',
        \ 'G:group',
        \ 'f:function',
        \ 'a:altstep',
        \ 't:type',
        \ 'P:modulepar',
        \ 'm:member',
        \ 'c:const',
        \ 'd:template',
        \ 's:signature',
        \ 'T:timer',
        \ 'p:port',
        \ 'e:enum'
    \ ]
\ }

